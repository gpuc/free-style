/******************************************************************************
GPU Computing / GPGPU Praktikum source code.

******************************************************************************/

#include "RayTracingTask.h"

#include "../Common/CLUtil.h"

#ifdef min // these macros are defined under windows, but collide with our math utility
#	undef min
#endif
#ifdef max
#	undef max
#endif

#include "HLSLEx.h"
#include "GLCommon.h"

#include <string>
#include <algorithm>

#include "CL/cl_gl.h"
#include "Scene.h"
#include <cassert>

#include <random>

std::default_random_engine generator;
std::uniform_real_distribution<double> distr(0.0, 1.0);

#define NUM_FORCE_LINES		4096
#define NUM_BANKS	32

using namespace std;
using namespace hlsl;

#ifdef _MSC_VER
// we would like to use fopen...
#pragma warning(disable: 4996)
// unreferenced local parameter: as some code is missing from the startup kit, we don't want the compiler complaining about these
#pragma warning(disable: 4100)
#endif

///////////////////////////////////////////////////////////////////////////////
// RayTracingTask

RayTracingTask::~RayTracingTask()
{
	ReleaseResources();
}

bool RayTracingTask::InitResources(cl_device_id Device, cl_context Context)
{
	m_scene = new Scene();
	unsigned int sphereCount = m_scene->m_sphereCount;

	cl_int clError;
	m_dPositions = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_float3), m_scene->m_positions, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load positions.");

	m_dEmissions = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_float3), m_scene->m_emissions, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load emissions.");

	m_dColors = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_float3), m_scene->m_colors, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load colors.");

	m_dRadii = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_float), m_scene->m_radii, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load radii.");

	m_dReflectionTypes = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_uint), m_scene->m_reflectionTypes, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load reflection types.");

	m_dRandoms = clCreateBuffer(Context, CL_MEM_READ_ONLY | CL_MEM_COPY_HOST_PTR, sphereCount * sizeof(cl_float), getRandoms(sphereCount), &clError);
	V_RETURN_FALSE_CL(clError, "Failed to load randoms.");

	glGenTextures(1, &m_resultTexture);
	glBindTexture(GL_TEXTURE_2D, m_resultTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 768, 0, GL_RGBA, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &m_tempTexture);
	glBindTexture(GL_TEXTURE_2D, m_tempTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 768, 0, GL_RGBA, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);

	glGenTextures(1, &m_frameTexture);
	glBindTexture(GL_TEXTURE_2D, m_frameTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA32F, 1024, 768, 0, GL_RGBA, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_2D, 0);

	m_dTextureRenderFrameWrite = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_frameTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureRenderFrameWrite.");

	m_dTextureCopyRenderRead = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_resultTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureCopyRenderRead.");

	m_dTextureCopyTempWrite = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_tempTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureCopyTempWrite.");

	m_dTextureAccumulateTempRead = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_tempTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureAccumulateTempRead.");

	m_dTextureAccumulateFrameRead = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_frameTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureAccumulateFrameRead.");

	m_dTextureAccumulateRenderWrite = clCreateFromGLTexture2D(Context, CL_MEM_READ_WRITE, GL_TEXTURE_2D, 0, m_resultTexture, &clError);
	V_RETURN_FALSE_CL(clError, "Failed to link m_dTextureAccumulateRenderWrite.");

	std::string sourceCode;
	CLUtil::LoadProgramSourceToMemory("PathTracing.cl", sourceCode);
	m_renderProgram = CLUtil::BuildCLProgramFromMemory(Device, Context, sourceCode);
	if (m_renderProgram == nullptr)
		return false;

	m_renderKernel = clCreateKernel(m_renderProgram, "render", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create kernel.");

	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 0, sizeof(cl_uint), &sphereCount), "Failed setting kernel arg: SphereCount");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 1, sizeof(cl_mem), &m_dPositions), "Failed setting kernel arg: Positions");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 2, sizeof(cl_mem), &m_dEmissions), "Failed setting kernel arg: Emissions");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 3, sizeof(cl_mem), &m_dColors), "Failed setting kernel arg: Colors");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 4, sizeof(cl_mem), &m_dRadii), "Failed setting kernel arg: Radii");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 5, sizeof(cl_mem), &m_dReflectionTypes), "Failed setting kernel arg: Reflection Types");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 6, sizeof(cl_mem), &m_dRandoms), "Failed setting kernel arg: Randoms");
	V_RETURN_FALSE_CL(clSetKernelArg(m_renderKernel, 7, sizeof(cl_mem), &m_dTextureRenderFrameWrite), "Failed setting kernel arg: Texture Render Frame Write");

	CLUtil::LoadProgramSourceToMemory("Accumulate.cl", sourceCode);
	m_accumulateProgram = CLUtil::BuildCLProgramFromMemory(Device, Context, sourceCode);
	if (m_accumulateProgram == nullptr)
		return false;

	m_copyKernel = clCreateKernel(m_accumulateProgram, "copy", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create copy kernel.");

	V_RETURN_FALSE_CL(clSetKernelArg(m_copyKernel, 0, sizeof(cl_mem), &m_dTextureCopyRenderRead), "Failed setting kernel arg: Texture Copy Render Read");
	V_RETURN_FALSE_CL(clSetKernelArg(m_copyKernel, 1, sizeof(cl_mem), &m_dTextureCopyTempWrite), "Failed setting kernel arg: Texture Copy Temp Write");

	m_accumulateKernel = clCreateKernel(m_accumulateProgram, "accumulate", &clError);
	V_RETURN_FALSE_CL(clError, "Failed to create accumulate kernel.");

	V_RETURN_FALSE_CL(clSetKernelArg(m_accumulateKernel, 0, sizeof(cl_mem), &m_dTextureAccumulateTempRead), "Failed setting kernel arg: Texture Accumulate Temp Read");
	V_RETURN_FALSE_CL(clSetKernelArg(m_accumulateKernel, 1, sizeof(cl_mem), &m_dTextureAccumulateFrameRead), "Failed setting kernel arg: Texture Accumulate Frame Read");
	V_RETURN_FALSE_CL(clSetKernelArg(m_accumulateKernel, 2, sizeof(cl_mem), &m_dTextureAccumulateRenderWrite), "Failed setting kernel arg: Texture Accumulate Render Write");

	return true;
}

void RayTracingTask::ReleaseResources()
{
	SAFE_RELEASE_MEMOBJECT(m_dPositions);
	SAFE_RELEASE_MEMOBJECT(m_dEmissions);
	SAFE_RELEASE_MEMOBJECT(m_dColors);
	SAFE_RELEASE_MEMOBJECT(m_dRadii);
	SAFE_RELEASE_MEMOBJECT(m_dReflectionTypes);
	SAFE_RELEASE_MEMOBJECT(m_dRandoms);
	SAFE_RELEASE_MEMOBJECT(m_dTextureRenderFrameWrite);
	SAFE_RELEASE_MEMOBJECT(m_dTextureCopyRenderRead);
	SAFE_RELEASE_MEMOBJECT(m_dTextureCopyTempWrite);
	SAFE_RELEASE_MEMOBJECT(m_dTextureAccumulateTempRead);
	SAFE_RELEASE_MEMOBJECT(m_dTextureAccumulateFrameRead);
	SAFE_RELEASE_MEMOBJECT(m_dTextureAccumulateRenderWrite);
	SAFE_RELEASE_KERNEL(m_renderKernel);
	SAFE_RELEASE_KERNEL(m_accumulateKernel);
	SAFE_RELEASE_KERNEL(m_copyKernel);
	SAFE_RELEASE_PROGRAM(m_renderProgram);
	SAFE_RELEASE_PROGRAM(m_accumulateProgram);
}

void RayTracingTask::ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3])
{
	glFinish();

	size_t globalWorkSize[2] = { 1024, 768 };
	LocalWorkSize[0] = 1;

	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureRenderFrameWrite, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");
	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureCopyRenderRead, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");
	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureCopyTempWrite, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");
	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureAccumulateTempRead, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");
	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureAccumulateFrameRead, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");
	V_RETURN_CL(clEnqueueAcquireGLObjects(CommandQueue, 1, &m_dTextureAccumulateRenderWrite, 0, nullptr, nullptr), "Error acquiring OpenGL buffer.");

	V_RETURN_CL(clEnqueueWriteBuffer(CommandQueue, m_dRandoms, true, 0, m_scene->m_sphereCount * sizeof(cl_float), getRandoms(m_scene->m_sphereCount), 0, nullptr, nullptr), "Failed to update randoms.");
	V_RETURN_CL(clEnqueueNDRangeKernel(CommandQueue, m_renderKernel, 2, nullptr, globalWorkSize, LocalWorkSize, 0, nullptr, nullptr), "Error executing render kernel.");

	V_RETURN_CL(clEnqueueNDRangeKernel(CommandQueue, m_copyKernel, 2, nullptr, globalWorkSize, LocalWorkSize, 0, nullptr, nullptr), "Error executing copy kernel.");

	V_RETURN_CL(clSetKernelArg(m_accumulateKernel, 3, sizeof(cl_uint), &m_frameNumber), "Failed setting kernel arg: Frame Texture");
	V_RETURN_CL(clEnqueueNDRangeKernel(CommandQueue, m_accumulateKernel, 2, nullptr, globalWorkSize, LocalWorkSize, 0, nullptr, nullptr), "Error executing accumulate kernel.");

	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureAccumulateRenderWrite, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");
	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureAccumulateFrameRead, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");
	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureAccumulateTempRead, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");
	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureCopyTempWrite, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");
	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureCopyRenderRead, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");
	V_RETURN_CL(clEnqueueReleaseGLObjects(CommandQueue, 1, &m_dTextureRenderFrameWrite, 0, nullptr, nullptr), "Error releasing OpenGL buffer.");

	clFinish(CommandQueue);

	m_frameNumber++;
}

void RayTracingTask::Render()
{
	glClearColor(0.5, 0.5, 0.5, 1);
	glClear(GL_COLOR_BUFFER_BIT);

	glBindTexture(GL_TEXTURE_2D, m_resultTexture);

	glBegin(GL_QUADS);
	glTexCoord2i(0, 1);
	glVertex3f(-1, 1, 0);
	glTexCoord2i(1, 1);
	glVertex3f(1, 1, 0);
	glTexCoord2i(1, 0);
	glVertex3f(1, -1, 0);
	glTexCoord2i(0, 0);
	glVertex3f(-1, -1, 0);
	glEnd();
}

void RayTracingTask::OnWindowResized(int Width, int Height)
{
    glViewport(0, 0, Width, Height);
}

cl_float* RayTracingTask::getRandoms(unsigned int amount)
{
	if (m_randoms == nullptr)
	{
		m_randoms = new cl_float[amount];
	}

	for (unsigned int i = 0; i < amount; i++)
	{
		m_randoms[i] = static_cast<float>(distr(generator));
	}

	return m_randoms;
}
