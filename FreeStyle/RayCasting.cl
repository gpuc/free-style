#define EPSILON 0.001F
#define WIDTH 1024
#define HEIGHT 768

struct Camera
{
	float3 position;
	float3 direction;
	float3 cx;
	float3 cy;
};

struct Ray
{
	float3 position;
	float3 direction;
};

struct RayHit
{
	float3 position;
	float3 normal;
	float3 nl;
	float distance;
	uint sphere;
};

struct Camera getCamera()
{
	struct Camera camera;
	camera.position = (float3)(50, 52, 295.6);
	camera.direction = normalize((float3)(0, -0.042612, -1));
	camera.cx = (float3)(WIDTH * 0.5135F / HEIGHT, 0, 0);
	camera.cy = normalize(cross(camera.cx, camera.direction)) * 0.5135F;
	return camera;
}

struct Ray createRay(struct Camera camera, int2 gid, float r1, float r2)
{
	r1 *= 2;
	r2 *= 2;

	float dx = r1 < 1 ? sqrt(r1) - 1 : 1 - sqrt(2 - r1);
	float dy = r2 < 1 ? sqrt(r2) - 1 : 1 - sqrt(2 - r2);

	float3 dir = camera.cx * (((0.5F + dx) / 2 + gid.x) / WIDTH - 0.5F) + camera.cy * (((0.5F + dy) / 2 + gid.y) / HEIGHT - 0.5F) + camera.direction;

	struct Ray ray;
	ray.position = camera.position + dir * 140;
	ray.direction = normalize(dir);
	return ray;
}

bool raySphereIntersection(struct Ray ray, float3 spherePosition, float sphereRadius, float *distance)
{
	float3 rayToSpherePosition = spherePosition - ray.position;
	float angle = dot(rayToSpherePosition, ray.direction);
	float det = angle * angle - dot(rayToSpherePosition, rayToSpherePosition) + sphereRadius * sphereRadius;
	if (det < 0)
		return false;

	det = sqrt(det);
	float d = angle - det;
	if (d > 1e-4)
	{
		*distance = d;
		return true;
	}

	d = angle + det;
	if (d > 1e-4)
	{
		*distance = d;
		return true;
	}

	return false;
}

bool cast(struct Ray ray, __constant float3 *positions, __constant float *radii, uint sphereCount, struct RayHit *hit)
{
	bool found = false;
	for (uint i = 0; i < 9; i++)
	{
		float distance;
		if (!raySphereIntersection(ray, positions[i], radii[i], &distance))
			continue;

		if (!found || distance < hit->distance)
		{
			hit->position = ray.position + ray.direction * distance;
			hit->normal = normalize(hit->position - positions[i]);
			hit->nl = dot(hit->normal, ray.direction) < 0 ? hit->normal : (hit->normal * -1);
			hit->distance = distance;
			hit->sphere = i;
		}

		found = true;
	}

	return found;
}

__kernel void render(uint sphereCount, __constant float3 *positions,
	__constant float3 *emissions, __constant float3 *colors,
	__constant float *radii, __constant uint *reflectionTypes,
	__constant float *randoms, __write_only image2d_t image)
{
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	struct Ray ray = createRay(getCamera(), gid, randoms[0], randoms[1]);
	float4 color = (float4)(0, 0, 0, 1);

	struct RayHit hit;
	if (cast(ray, positions, radii, sphereCount, &hit))
	{
		color = (float4)(colors[hit.sphere].x, colors[hit.sphere].y, colors[hit.sphere].z, 1);
	}

	write_imagef(image, gid, color);
}
