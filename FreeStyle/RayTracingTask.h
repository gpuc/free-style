/******************************************************************************
                         .88888.   888888ba  dP     dP 
                        d8'   `88  88    `8b 88     88 
                        88        a88aaaa8P' 88     88 
                        88   YP88  88        88     88 
                        Y8.   .88  88        Y8.   .8P 
                         `88888'   dP        `Y88888P' 
                                                       
                                                       
   a88888b.                                         dP   oo                   
  d8'   `88                                         88                        
  88        .d8888b. 88d8b.d8b. 88d888b. dP    dP d8888P dP 88d888b. .d8888b. 
  88        88'  `88 88'`88'`88 88'  `88 88    88   88   88 88'  `88 88'  `88 
  Y8.   .88 88.  .88 88  88  88 88.  .88 88.  .88   88   88 88    88 88.  .88 
   Y88888P' `88888P' dP  dP  dP 88Y888P' `88888P'   dP   dP dP    dP `8888P88 
                                88                                        .88 
                                dP                                    d8888P  
******************************************************************************/

#ifndef _RAYTRACING_TASK_H
#define _RAYTRACING_TASK_H

#include "../Common/IGUIEnabledComputeTask.h"

#include "Scene.h"
#include "GLCommon.h"


class RayTracingTask : public IGUIEnabledComputeTask
{
public:
	RayTracingTask() = default;

	virtual ~RayTracingTask();

	// IComputeTask
	virtual bool InitResources(cl_device_id Device, cl_context Context);

	virtual void ReleaseResources();

	virtual void ComputeGPU(cl_context Context, cl_command_queue CommandQueue, size_t LocalWorkSize[3]);

	// Not implemented!
	virtual void ComputeCPU() {};
	virtual bool ValidateResults() {return false;};

	// IGUIEnabledComputeTask
	virtual void Render();

	virtual void OnKeyboard(int Key, int Action) {};

	virtual void OnMouse(int Button, int State) {};

	virtual void OnMouseMove(int X, int Y) {};

	virtual void OnIdle(double, float) {};

	virtual void OnWindowResized(int Width, int Height);

protected:
	cl_float *getRandoms(unsigned int amount);

	cl_program m_renderProgram = nullptr;
	cl_kernel m_renderKernel = nullptr;

	cl_program m_accumulateProgram = nullptr;
	cl_kernel m_accumulateKernel = nullptr;
	cl_kernel m_copyKernel = nullptr;

	cl_mem m_dPositions = nullptr;
	cl_mem m_dEmissions = nullptr;
	cl_mem m_dColors = nullptr;
	cl_mem m_dRadii = nullptr;
	cl_mem m_dReflectionTypes = nullptr;
	cl_mem m_dRandoms = nullptr;

	cl_mem m_dTextureRenderFrameWrite = nullptr;
	cl_mem m_dTextureCopyRenderRead = nullptr;
	cl_mem m_dTextureCopyTempWrite = nullptr;
	cl_mem m_dTextureAccumulateTempRead = nullptr;
	cl_mem m_dTextureAccumulateFrameRead = nullptr;
	cl_mem m_dTextureAccumulateRenderWrite = nullptr;

	GLuint m_resultTexture;
	GLuint m_frameTexture;
	GLuint m_tempTexture;

	Scene *m_scene = nullptr;
	cl_float *m_randoms = nullptr;

	cl_uint m_frameNumber = 0;
};

#endif // _RAYTRACING_TASK_H
