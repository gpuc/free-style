#ifndef _SCENE_H
#define _SCENE_H

#include <string>
#include "Structs.h"

class Scene
{
public:
	Scene(std::string fileName);
	~Scene();

	Triangle *getTriangles() const;
	size_t getTriangleCount() const;

	Material *getMaterials() const;
	size_t getMaterialCount() const;

	Viewport *getViewport();

	Light *getLights() const;
	size_t getLightCount() const;

private:
	void loadObj(std::string fileName);

	template <typename T>
	T streamProperty(std::string s);

	template <typename T>
	std::string toString(T &t);

	Triangle* m_triangles;
	size_t m_triangleCount;

	Material *m_materials;
	size_t m_materialCount;

	Viewport m_viewport;

	Light *m_lights;
	size_t m_lightCount;
};

#endif
