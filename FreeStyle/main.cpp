/******************************************************************************
GPU Computing / GPGPU Praktikum source code.

******************************************************************************/

#include "FreeStyleAssignment.h"

#include <iostream>

using namespace std;


int main(int argc, char** argv)
{
	FreeStyleAssignment* pAssignment = FreeStyleAssignment::GetSingleton();

	if(pAssignment)
	{
		pAssignment->EnterMainLoop(argc, argv);
		delete pAssignment;
	}
	
#ifdef _MSC_VER
	cout<<"Press any key..."<<endl;
	cin.get();
#endif

}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////


