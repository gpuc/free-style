__kernel void copy(__read_only image2d_t input, __write_only image2d_t output)
{
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	write_imagef(output, gid, read_imagef(input, gid));
}

__kernel void accumulate(__read_only image2d_t temp,
	__read_only image2d_t frame,
	__read_write image2d_t result,
	uint i)
{
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	float4 data = read_imagef(temp, gid);
	float4 current = read_imagef(frame, gid);
	write_imagef(result, gid, (data * i + current) / (i + 1));
}
