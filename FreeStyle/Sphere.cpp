#define TINYOBJLOADER_IMPLEMENTATION

#include "Scene.h"
#include <experimental/filesystem>
#include <regex>

Scene::Scene(std::string fileName)
{
	std::fstream stream(fileName, std::fstream::in);
	std::string content((std::istreambuf_iterator<char>(stream)), std::istreambuf_iterator<char>());
	std::regex regex("\\s*(\\w+)\\s*=\\s*(.*?)\\s*\n");
	std::sregex_iterator it(content.begin(), content.end(), regex);

	std::map<std::string, std::string> properties;
	for (std::sregex_iterator i = it; i != std::sregex_iterator(); ++i)
	{
		std::smatch match = *i;
		properties[match[1].str()] = match[2].str();
	}

	std::experimental::filesystem::path objPath(fileName);
	objPath.remove_filename();
	objPath.append(properties["obj_file"]);
	loadObj(objPath.string());

	m_viewport.cameraPos.x = streamProperty<cl_float>(properties["camera_position_x"]);
	m_viewport.cameraPos.y = streamProperty<cl_float>(properties["camera_position_y"]);
	m_viewport.cameraPos.z = streamProperty<cl_float>(properties["camera_position_z"]);

	m_viewport.cameraDir.x = streamProperty<cl_float>(properties["camera_look_x"]);
	m_viewport.cameraDir.y = streamProperty<cl_float>(properties["camera_look_y"]);
	m_viewport.cameraDir.z = streamProperty<cl_float>(properties["camera_look_z"]);

	m_viewport.fov = streamProperty<cl_float>(properties["fov"]);
	m_viewport.width = streamProperty<cl_int>(properties["resolution_x"]);
	m_viewport.height = streamProperty<cl_int>(properties["resolution_y"]);

	m_lightCount = streamProperty<size_t>(properties["lights"]);
	m_lights = new Light[m_lightCount];

	for (unsigned int i = 0; i < m_lightCount; i++)
	{
		m_lights[i].pos.x = streamProperty<cl_float>(properties["light_position_x_" + toString(i)]);
		m_lights[i].pos.y = streamProperty<cl_float>(properties["light_position_y_" + toString(i)]);
		m_lights[i].pos.z = streamProperty<cl_float>(properties["light_position_z_" + toString(i)]);

		m_lights[i].color.x = streamProperty<cl_float>(properties["light_color_x_" + toString(i)]);
		m_lights[i].color.y = streamProperty<cl_float>(properties["light_color_y_" + toString(i)]);
		m_lights[i].color.z = streamProperty<cl_float>(properties["light_color_z_" + toString(i)]);
	}
}

Scene::~Scene()
{
	if (m_triangles != nullptr)
	{
		delete[] m_triangles;
		m_triangles = nullptr;
	}

	if (m_materials != nullptr)
	{
		delete[] m_materials;
		m_materials = nullptr;
	}
}

Triangle* Scene::getTriangles() const
{
	return m_triangles;
}

size_t Scene::getTriangleCount() const
{
	return m_triangleCount;
}

Material* Scene::getMaterials() const
{
	return m_materials;
}

size_t Scene::getMaterialCount() const
{
	return m_materialCount;
}

Viewport* Scene::getViewport()
{
	return &m_viewport;
}

Light* Scene::getLights() const
{
	return m_lights;
}

size_t Scene::getLightCount() const
{
	return m_lightCount;
}

void Scene::loadObj(std::string fileName)
{
	std::experimental::filesystem::path path(fileName);
	path.remove_filename();

	tinyobj::attrib_t attribute;
	std::vector<tinyobj::shape_t> shapes;
	std::vector<tinyobj::material_t> materials;

	std::string warning;
	std::string error;
	bool result = tinyobj::LoadObj(&attribute, &shapes, &materials, &warning, &error, fileName.c_str(), path.string().c_str());
	if (!result)
	{
		assert(false);
		return;
	}

	m_triangleCount = 0;
	for (tinyobj::shape_t shape : shapes)
	{
		m_triangleCount += shape.mesh.num_face_vertices.size();
	}
	m_triangles = new Triangle[m_triangleCount];

	unsigned int i = 0;
	for (tinyobj::shape_t shape : shapes)
	{
		unsigned int j = 0;
		unsigned int m = 0;
		for (unsigned char vertexCount : shape.mesh.num_face_vertices)
		{
			float x[3];
			float y[3];
			float z[3];

			for (unsigned int vertex = 0; vertex < vertexCount; vertex++)
			{
				tinyobj::index_t index = shape.mesh.indices[j + vertex];
				x[vertex] = attribute.vertices[3 * index.vertex_index];
				y[vertex] = attribute.vertices[3 * index.vertex_index + 1];
				z[vertex] = attribute.vertices[3 * index.vertex_index + 2];
			}

			float e1x = x[1] - x[0];
			float e1y = y[1] - x[0];
			float e1z = z[1] - x[0];

			float e2x = x[2] - x[0];
			float e2y = y[2] - x[0];
			float e2z = z[2] - x[0];

			float cx = e2y * e1z - e2z * e1y;
			float cy = e2z * e1x - e2x * e1z;
			float cz = e2x * e1y - e2y * e1x;

			float length = sqrt(cx * cx + cy * cy + cz * cz);
			if (length != 0.F)
			{
				cx /= length;
				cy /= length;
				cz /= length;
			}

			Triangle &triangle = m_triangles[i];
			triangle.v0.x = x[0];
			triangle.v0.y = y[0];
			triangle.v0.z = z[0];

			triangle.v1.x = x[1];
			triangle.v1.y = y[1];
			triangle.v1.z = z[1];

			triangle.v2.x = x[2];
			triangle.v2.y = y[2];
			triangle.v2.z = z[2];

			triangle.normal.x = cx;
			triangle.normal.y = cy;
			triangle.normal.z = cz;

			triangle.material.x = shape.mesh.material_ids[m++];

			j += vertexCount;
			i++;
		}
	}

	m_materialCount = materials.size();
	m_materials = new Material[m_materialCount];

	i = 0;
	for (tinyobj::material_t &material : materials)
	{
		m_materials[i].color.x = material.diffuse[0];
		m_materials[i].color.y = material.diffuse[1];
		m_materials[i].color.z = material.diffuse[2];
		m_materials[i].color.w = 1.F;

		i++;
	}
}

template <typename T>
T Scene::streamProperty(std::string s)
{
	std::stringstream stream(s);
	T t;
	stream >> t;
	return t;
}

template <typename T>
std::string Scene::toString(T& t)
{
	std::stringstream stream;
	stream << t;
	return stream.str();
}

