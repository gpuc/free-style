#ifndef _SCENE_H
#define _SCENE_H

#include <CL/cl_platform.h>

class Scene
{
public:
	Scene();
	~Scene();

	cl_float3 *m_positions;
	cl_float3 *m_emissions;
	cl_float3 *m_colors;
	cl_float *m_radii;
	cl_uint *m_reflectionTypes;

	unsigned int m_sphereCount;
};

#endif
