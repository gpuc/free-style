#define PI 3.1415926535F
#define EPSILON 0.001F
#define WIDTH 1024
#define HEIGHT 768

struct Camera
{
	float3 position;
	float3 direction;
	float3 cx;
	float3 cy;
};

struct Ray
{
	float3 position;
	float3 direction;
};

struct RayHit
{
	float3 position;
	float3 normal;
	float3 normalOriented;
	float distance;
	uint sphere;
};

// camera for scene (from smallpt)
struct Camera getCamera()
{
	struct Camera camera;
	camera.position = (float3)(50, 52, 295.6);
	camera.direction = normalize((float3)(0, -0.042612, -1));
	camera.cx = (float3)(WIDTH * 0.5135F / HEIGHT, 0, 0);
	camera.cy = normalize(cross(camera.cx, camera.direction)) * 0.5135F;
	return camera;
}

// creates a ray through the given pixel with some randomness like in smallpt
struct Ray createRay(struct Camera camera, int2 gid, float r1, float r2)
{
	r1 *= 2;
	r2 *= 2;

	float dx = r1 < 1 ? sqrt(r1) - 1 : 1 - sqrt(2 - r1);
	float dy = r2 < 1 ? sqrt(r2) - 1 : 1 - sqrt(2 - r2);

	float3 dir = camera.cx * (((0.5F + dx) / 2 + gid.x) / WIDTH - 0.5F) + camera.cy * (((0.5F + dy) / 2 + gid.y) / HEIGHT - 0.5F) + camera.direction;

	struct Ray ray;
	ray.position = camera.position + dir * 140;
	ray.direction = normalize(dir);
	return ray;
}

// intersects a ray with a sphere
bool raySphereIntersection(struct Ray ray, float3 spherePosition, float sphereRadius, float *distance)
{
	float3 rayToSpherePosition = spherePosition - ray.position;
	float angle = dot(rayToSpherePosition, ray.direction);
	float det = angle * angle - dot(rayToSpherePosition, rayToSpherePosition) + sphereRadius * sphereRadius;
	if (det < 0)
		return false;

	det = sqrt(det);
	float d = angle - det;
	if (d > 1e-4)
	{
		*distance = d;
		return true;
	}

	d = angle + det;
	if (d > 1e-4)
	{
		*distance = d;
		return true;
	}

	return false;
}

bool cast(struct Ray ray, __constant float3 *positions, __constant float *radii, uint sphereCount, struct RayHit *hit)
{
	bool found = false;
	for (uint i = 0; i < sphereCount; i++)
	{
		float distance;
		if (!raySphereIntersection(ray, positions[i], radii[i], &distance))
			continue;

		if (!found || distance < hit->distance)
		{
			hit->position = ray.position + ray.direction * distance;
			hit->normal = normalize(hit->position - positions[i]);
			hit->normalOriented = dot(hit->normal, ray.direction) < 0 ? hit->normal : (hit->normal * -1);
			hit->distance = distance;
			hit->sphere = i;
		}

		found = true;
	}

	return found;
}

// random point on light source for Next Event Estimation
float3 getRandomPointOnLightSource(float r0, float r1)
{
	float x = 50;
	float z = 81.6F;
	float y = z - 0.27F;
	return (float3)(x - 5 + 10 * r0, y, z - 5 + 10 * r1);
}

// solve rendering equation
float3 radiance(struct Ray ray, int depth,
	uint sphereCount, __constant float3 *positions,
	__constant float3 *emissions, __constant float3 *colors,
	__constant float *radii, __constant uint *reflectionTypes,
	__constant float *randoms)
{
	struct RayHit hit;
	if (!cast(ray, positions, radii, sphereCount, &hit)) // cast ray into scene and check for hits
		return (float3)(0, 0, 0); // if nothing has been hit, return black

	if (depth >= 5) // check if maximum depth has been reached
		return emissions[hit.sphere];

	// Next Event Estimation
	float3 lightSourcePos = getRandomPointOnLightSource(randoms[4], randoms[5]); // random light source point
	float3 lightSourceDir = normalize(lightSourcePos - hit.position); // direcion from hit position to light source
	float3 lightSourceNormal = normalize(lightSourcePos - positions[8]); // normal of light source
	float3 lightSourceNormalOriented = dot(lightSourceNormal, lightSourceDir) < 0 ? lightSourceNormal : (lightSourceNormal * -1); // normal on light source, which is on the correct side
	float lightSourceDist = length(lightSourcePos - hit.position); // distance from hit position to light source
	
	float3 directLightContribution = (float3)(0, 0, 0);
	if (dot(hit.normal, lightSourceDir) > 0 && dot(lightSourceNormalOriented, -lightSourceDir) > 0)
	{
		float3 em = dot(lightSourceNormalOriented, -lightSourceDir) * emissions[8] / (lightSourceDist * lightSourceDist);
		directLightContribution = colors[8] * em * dot(hit.normal, lightSourceDir);
	}

	float rand = randoms[hit.sphere];

	if (reflectionTypes[hit.sphere] == 0) // diffuse
	{
		float3 axis = fabs(hit.normalOriented.x) > 0.1F ? ((float3)(0, 1, 0)) : ((float3)(1, 0, 0));
		float3 perpendicularNormal1 = normalize(cross(axis, hit.normal)); // 1st perpendicular vector to oriented hit normal
		float3 perpendicularNormal2 = cross(hit.normalOriented, perpendicularNormal1); // 2nd perpendicular vector to oriented hit normal

		float angle = 2 * PI * rand; // select a random angle to reflect to
		float randRoot = sqrt(rand);

		struct Ray diffuseRay;
		diffuseRay.position = hit.position;
		diffuseRay.direction = normalize(perpendicularNormal1 * cos(angle) * randRoot + perpendicularNormal2 * sin(angle) * randRoot + hit.normalOriented * sqrt(1 - rand));
		return emissions[hit.sphere] + colors[hit.sphere] * radiance(diffuseRay, depth + 1, sphereCount, positions, emissions, colors, radii, reflectionTypes, randoms) + directLightContribution;
	}

	struct Ray reflectionRay;
	reflectionRay.position = hit.position;
	reflectionRay.direction = ray.direction - 2 * hit.normal * dot(hit.normal, ray.direction); // perfect specular reflection direction
	float3 reflectionRadiance = radiance(reflectionRay, depth + 1, sphereCount, positions, emissions, colors, radii, reflectionTypes, randoms);

	if (reflectionTypes[hit.sphere] == 1) // specular
	{
		return emissions[hit.sphere] + colors[hit.sphere] * reflectionRadiance + directLightContribution;
	}

	// reflection and refraction
	bool isRefraction = dot(hit.normal, hit.normalOriented) > 0;
	float refractionIndexAir = 1;
	float refractionIndexGlass = 1.5F;
	float refractionRatio = isRefraction ? refractionIndexAir / refractionIndexGlass : refractionIndexGlass / refractionIndexAir;
	float angle = dot(ray.direction, hit.normalOriented);
	
	float cos2t = 1 - refractionRatio * refractionRatio * (1 - angle * angle);
	if (cos2t < 0) // total internal reflection
		return emissions[hit.sphere] + colors[hit.sphere] * reflectionRadiance + directLightContribution;

	float3 transmissionDirection = normalize(ray.direction * refractionRatio - hit.normal * ((isRefraction ? 1 : -1) * (angle * refractionRatio + sqrt(cos2t))));
	float a = refractionIndexGlass - refractionIndexAir;
	float b = refractionIndexGlass + refractionIndexAir;
	float F0 = a * a / (b * b);
	float cosT = 1 - (isRefraction ? -angle : dot(transmissionDirection, hit.normal));
	float Fr = F0 + (1 - F0) * cosT * cosT * cosT * cosT * cosT; // Fresnel reflectance
	float reflectionPercentage = Fr / (0.25F + 0.5 * Fr);
	float transmissionPercentage = (1 - Fr) / (1 - (0.25F + 0.5 * Fr));

	struct Ray refractionRay;
	refractionRay.position = hit.position;
	refractionRay.direction = transmissionDirection;

	float3 refractionRadiance = radiance(refractionRay, depth + 1, sphereCount, positions, emissions, colors, radii, reflectionTypes, randoms);
	return emissions[hit.sphere] + colors[hit.sphere] * (reflectionRadiance * reflectionPercentage + refractionRadiance * transmissionPercentage) + directLightContribution;
}

__kernel void render(uint sphereCount, __constant float3 *positions,
	__constant float3 *emissions, __constant float3 *colors,
	__constant float *radii, __constant uint *reflectionTypes,
	__constant float *randoms, __write_only image2d_t image)
{
	int2 gid;
	gid.x = get_global_id(0);
	gid.y = get_global_id(1);

	float3 color = radiance(createRay(getCamera(), gid, randoms[0], randoms[1]), 0, sphereCount, positions, emissions, colors, radii, reflectionTypes, randoms);
	write_imagef(image, gid, (float4)(color.x, color.y, color.z, 1));
}
