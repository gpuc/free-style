#include "Scene.h"

// See smallpt example scene
Scene::Scene()
{
	m_sphereCount = 9;

	m_positions = new cl_float3[m_sphereCount];
	m_emissions = new cl_float3[m_sphereCount];
	m_colors = new cl_float3[m_sphereCount];
	m_radii = new cl_float[m_sphereCount];
	m_reflectionTypes = new cl_uint[m_sphereCount];

	m_radii[0] = 1e5; // left
	m_positions[0] = { 1e5 + 1, 40.8, 81.6 };
	m_emissions[0] = { 0, 0, 0 };
	m_reflectionTypes[0] = 0;
	m_colors[0] = { .75, .25, .75 };

	m_radii[1] = 1e5; // right
	m_positions[1] = { -1e5 + 99,40.8,81.6 };
	m_emissions[1] = { 0, 0, 0 };
	m_reflectionTypes[1] = 0;
	m_colors[1] = { .25, .25, .75 };

	m_radii[2] = 1e5; // back
	m_positions[2] = { 50,40.8, 1e5 };
	m_emissions[2] = { 0, 0, 0 };
	m_reflectionTypes[2] = 0;
	m_colors[2] = { .75, .75, .75 };

	m_radii[3] = 1e5; // front
	m_positions[3] = { 50,40.8,-1e5 + 170 };
	m_emissions[3] = { 0, 0, 0 };
	m_reflectionTypes[3] = 0;
	m_colors[3] = { 0, 0, 0 };

	m_radii[4] = 1e5; // bottom
	m_positions[4] = { 50, 1e5, 81.6 };
	m_emissions[4] = { 0, 0, 0 };
	m_reflectionTypes[4] = 0;
	m_colors[4] = { .75,.75,.75 };

	m_radii[5] = 1e5; // top
	m_positions[5] = { 50,-1e5 + 81.6,81.6 };
	m_emissions[5] = { 0, 0, 0 };
	m_reflectionTypes[5] = 0;
	m_colors[5] = { .75,.75,.75 };

	m_radii[6] = 16.5; // mirror
	m_positions[6] = { 27,16.5,47 };
	m_emissions[6] = { 0, 0, 0 };
	m_reflectionTypes[6] = 1;
	m_colors[6] = { .999, .999, .999 };

	m_radii[7] = 16.5; // glass
	m_positions[7] = { 73,16.5,78 };
	m_emissions[7] = { 0, 0, 0 };
	m_reflectionTypes[7] = 2;
	m_colors[7] = { .999, .999, .999 };

	m_radii[8] = 600; // light
	m_positions[8] = { 50,681.6 - .27,81.6 };
	m_emissions[8] = { 12, 12, 12 };
	m_reflectionTypes[8] = 0;
	m_colors[8] = { 0, 0, 0 };
}

Scene::~Scene()
{
	if (m_positions != nullptr)
	{
		delete[] m_positions;
		m_positions = nullptr;
	}

	if (m_emissions != nullptr)
	{
		delete[] m_emissions;
		m_emissions = nullptr;
	}

	if (m_colors != nullptr)
	{
		delete[] m_colors;
		m_colors = nullptr;
	}

	if (m_radii != nullptr)
	{
		delete[] m_radii;
		m_radii = nullptr;
	}

	if (m_reflectionTypes != nullptr)
	{
		delete[] m_reflectionTypes;
		m_reflectionTypes = nullptr;
	}
}
